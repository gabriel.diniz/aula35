public class aula35 {  // condição ternária

    public static void main(String[] args) {

        int a, b; // variaveis
       // String valor;  // string
        a = 5;  // valor de variavel
        b = 6;  // valor de variavel
        /*
        if(a==b)
            valor = "verdadeiro";
        else
            valor = "falso";
           */
        String valor = (a>b) ? "verdadeiro" : "false";  // declara tipo string valor, A maior do que B ?  verifica condição, se for verdadeira se não falsa

        System.out.println(valor);  // imprimi mensagem valor
    }
}
